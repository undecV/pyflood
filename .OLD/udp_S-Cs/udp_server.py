#!/usr/bin/python
# -*- coding: UTF-8 -*-

import socket

import local_ip

SERVER_ADDR = local_ip.local_ip()
SERVER_PORT = 19487
RECEIVE_BUFFER_SIZE = 1024  # 接收用的 Buffer 大小


def server(host, port):
    # IPv4 + UDP
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((host, port))
    print("Listening: {0}:{1}".format(host, port))

    c = 0  # Counter
    # Server loop.
    while True:
        c = c + 1  # Counter
        try:
            rdata, client = s.recvfrom(RECEIVE_BUFFER_SIZE)
            print("{0:02d} | {1}:{2} said: {3}".format(c, client[0], client[1], rdata.decode("UTF-8")))
            s.sendto("Nanodesu!..~".encode("UTF-8"), client)
        except IOError:
            print("{0:02d} | IOError".format(c))


def main():
    server(SERVER_ADDR, SERVER_PORT)


if __name__ == '__main__':
    main()
