#!/usr/bin/python
# -*- coding: UTF-8 -*-

import socket


SERVER_ADDR = "192.168.0.201"
SERVER_PORT = 9487
# RECEIVE_BUFFER_SIZE = 1024      # 接收用的 Buffer 大小
MSG = "PoiPoiPoi..~"


def client(msg, host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server = (host, port)
    s.sendto(msg.encode("UTF-8"), server)


def main():
    client(MSG, SERVER_ADDR, SERVER_PORT)


if __name__ == '__main__':
    main()
