#!/usr/bin/python
# -*- coding: UTF-8 -*-

# Build-in
import socket
import sys
# Mine
from common.local_ip import local_ip


SERVER_ADDR = local_ip()
SERVER_PORT = 19487
RECEIVE_BUFFER_SIZE = 1024  # 接收用的 Buffer 大小


SUB0 = [["192.168.0.201", 19488],
        ["192.168.0.201", 19489]]
SUB1 = []
SUB2 = []


def node(host, port, subs):
    # IPv4 + UDP
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((host, port))
    print("Listening: {0}:{1}".format(host, port))

    c = 0  # Counter
    # Server loop.
    while True:
        c = c + 1  # Counter
        try:
            rdata, client = s.recvfrom(RECEIVE_BUFFER_SIZE)
            print("{0:02d} | {1}:{2} said: {3}".format(c, client[0], client[1], rdata.decode("UTF-8")))
            for sub in subs:
                s.sendto(rdata.encode("UTF-8"), (sub[0], sub[1]))
        except IOError:
            print("{0:02d} | IOError".format(c))


def main(argv):
    port = 0
    subs = []
    if argv[1] == "0":
        port = 19487
        subs = SUB0
    elif argv[1] == "1":
        port = 19488
        subs = SUB1
    elif argv[1] == "2":
        port = 19489
        subs = SUB2
    else:
        exit(-1)

    node(SERVER_ADDR, port, subs)


if __name__ == '__main__':
    main(sys.argv)
