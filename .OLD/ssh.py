# pip
import paramiko
# Mine
import ssh_explosion


def ssh_explosion_and_exec(ip, port=22, cmds=None, timeout=1):
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    _pass = ssh_explosion.ssh_explosion(ip, port, ssh_client=ssh_client, timeout=timeout)
    if _pass is None:
        print("UNKNOWN ID AND PASS.")
        return

    if cmds is None:
        cmds = ["uptime"]
    cmd_str = ';'.join(cmds)

    stdin, stdout, stderr = ssh_client.exec_command(cmd_str)
    print(stdout.read().decode("UTF-8"))

    ssh_client.close()


def main():
    ip = "192.168.0.205"
    port = 22
    timeout = 1
    cmds = ["uptime", "ls -al \Downloads"]

    ssh_explosion_and_exec(ip, port, cmds, timeout)


if __name__ == '__main__':
    main()
