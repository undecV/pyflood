#!/usr/bin/python
# -*- coding: UTF-8 -*-

# Mine
from common.ip import *


# Command format:
#       METHOD TARGET PORT TIME (SIZE)


class FloodArgs:
    def __init__(self, args):
        self.host = ""
        self.port = 0
        self.time = 0
        self.size = 0
        if len(args) != 0:
            self.parse(args)

    def __str__(self):
        s = "TARGET: {}:{}, ".format(self.host, self.port)
        s += "TIME: {}, ".format(self.time)
        s += "SIZE: {}".format(self.size)
        return s

    def parse(self, args):
        if len(args) < 3:
            raise SyntaxError
        if validate_ipv4(args[0]) is False:
            raise SyntaxError
        if args[1].isnumeric() and args[2].isnumeric() is False:
            raise SyntaxError
        if len(args) == 4:
            if args[3].isnumeric() is False:
                raise SyntaxError
            else:
                self.size = int(args[3])
        self.host = args[0]
        self.port = int(args[1])
        self.time = int(args[2])
