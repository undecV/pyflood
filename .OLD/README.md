


# README #

Wow, thx for your reading...


### Files

- 攻擊程式
    - `client.py`：攻擊程式的 **Main** 主程式
    - `flood.py`：攻擊程式的 Flooding 攻擊部分
    - `scan.py`：攻擊程式的 Scanning 部分
    - `server.py`：TCP Server，用於測試攻擊程式
    - `ssh.py`：SSH 弱口令侵入
    - 指令系統
        - `qmanager.py`: 取得指令并且執行
        - `cmds.py`: 物件 'Cmd' 和指令的 parser
        - `method.py`: 執行的方法

- `.\udp_S-Cs\` UDP Server-Client 實現
    - `node.py`：UDP 指令傳遞節點
    - `udp_client.py`：UDP Client
    - `udp_server.py`：UDP Server

- `.\common\` 通用的：常用函數大雜燴
    - `common.sock`：用於找到自己的 IP 地址
    - `common.ip`: 檢查字串是否是 IP 的格式
