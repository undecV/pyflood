#!/usr/bin/python
# -*- coding: UTF-8 -*-

import scan


def grp_scan(hosts, ports_grps, arguments='-T4', sudo=False):
    targets = scan.scan(hosts, ungrp(ports_grps), arguments, sudo)
    targets_grp = engrp(ports_grps, targets)
    return targets_grp


def ungrp(grps):
    ls = []
    for grp in grps:
        for e in grp:
            ls.append(e)
    return ls


def engrp(ports_grps, targets):
    targets_grp = []
    for _ in ports_grps:
        targets_grp.append([])

    for target in targets:
        for grp in ports_grps:
            if target[1] in grp:
                targets_grp[ports_grps.index(grp)].append(target)
    return targets_grp
