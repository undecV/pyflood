#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys

import grp_scan


DURATION_DEFAULT = 1  # 持續時間，秒


def main(_):
    print("Scanning...")
    targets_grp = grp_scan.grp_scan('192.168.1.0/24', [[80, 8080],[19487]], '-T4')
    targets = targets_grp[0]
    friends = targets_grp[1]
    print(targets)
    print(friends)


if __name__ == '__main__':
    main(sys.argv)
