#!/usr/bin/python
# -*- coding: UTF-8 -*-

# Build-in
import time
# pip
import nmap  # pip install python-nmap


# Scan targets using nmap.
def scan(hosts, ports, arguments="-T4", sudo=False):
    """
    使用 nmap 掃描。

    :param hosts: 目標 IP 區段。
    :param ports: 目標 Ports 列表。
    :param arguments: nmap 參數。
    :param sudo: 使用 Root 權限掃描。
    :return: 掃描結果。
    """

    # - Private IPv4 address spaces:
    #     10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16
    # - nmap useage:
    #     -T <Paranoid|Sneaky|Polite|Normal|Aggressive|Insane>

    # Int list to str, Ex: [22, 80] -> "22,80"
    ports_str = ','.join(str(e) for e in ports)

    # Scan
    nm = nmap.PortScanner()
    tic = time.time()
    nm.scan(hosts, ports_str, arguments, sudo)
    toc = time.time() - tic
    print("tic-toc: {0}".format(toc))
    # print(nm.command_line())

    # Get result
    targets = []
    trans_layer = "tcp"
    for argv in arguments.split(' '):
        if argv == "-sU":
            trans_layer = "udp"

    for host in nm.all_hosts():
        for port in ports:
            if nm[host][trans_layer][port]["state"] == "open":
                targets.append([host, port])
    return targets


def targets_filter(targets, whitelist):
    """
    目標過濾器。

    :param targets: 掃描結果，scan.scan() 的返回值。
    :param whitelist: 白名單。
    :return: 過濾後的目標。
    """
    targets_filtered = []
    for target in targets:
        if target[0] not in whitelist:
            targets_filtered.append(target)
    return targets_filtered
