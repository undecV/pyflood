# Build-in
import socket
# Mine
import methods


# Command format:
#       METHOD TARGET PORT TIME (SIZE)
#       PING blablabla...


def cmd_split(cmd_str):
    part = cmd_str.split(" ")
    method = part[0]
    args = part[1:]
    return method, args


def main():
    cmd_s = "PING poi"
    method, args = cmd_split(cmd_s)
    print(method, args)
    if method not in methods.lst:
        raise SyntaxError("Command.")
    methods.lst[method](args)


if __name__ == '__main__':
    main()
