def get_ifs_ipv4():
    """
    Get IPv4 info for all interfaces.
    Need module: netifaces
        $ pip install netifaces
    :return: a list of each interface info: ["if_name", IPv4Address(self_ip), IPv4Network(self_subnet)]
    """
    import ipaddress
    import netifaces  # pip install netifaces

    # Get all information for all interfaces.
    _ifs_info = []
    for _if_name in netifaces.interfaces():
        _if_info = netifaces.ifaddresses(_if_name)
        # Filter others except IPv4.
        if netifaces.AF_INET in _if_info:
            # Return: A list of [if_name, IPv4Address, IPv4Network]
            sub = "{}/{}".format(_if_info[netifaces.AF_INET][0]['addr'], _if_info[netifaces.AF_INET][0]['netmask'])
            _ifs_info.append([_if_name,
                              ipaddress.IPv4Address(_if_info[netifaces.AF_INET][0]['addr']),
                              ipaddress.IPv4Network(sub, strict=False)])

    # Filter loop back.
    _ifs_info = filter(lambda _if: not _if[2].is_loopback, _ifs_info)

    return list(_ifs_info)


if __name__ == '__main__':
    _ifs = get_ifs_ipv4()
    print(_ifs)
    # print(list(_ifs[0][2].hosts()))
