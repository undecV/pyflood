#!/usr/bin/python
# -*- coding: UTF-8 -*-

# usage: flood.py [-h] host port [time]
#
# Server stress testing.
#
# positional arguments:
#   host        Target ip.
#   port        Target port.
#   time        Duration time, sec, default: 10s.
#
# optional arguments:
#   -h, --help  show this help message and exit

# Build-in
import time
import socket

# RECEIVE_BUFFER_SIZE = 1024      # 接收用的 Buffer 大小
# DURATION_DEFAULT = 1            # 持續時間，秒


# HTTP GET PACKAGE
def http_get(host):
    """
    生成 HTTP GET 封包內容。

    :str host: 目標主機。
    :return: 封包內容。
    """
    content = "GET / HTTP/1.1\r\n" + \
              "Host: {0}\r\n".format(host) + \
              "Connection: close\r\n" + \
              "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0\r\n" + \
              "Accept: text/html, application/xhtml+xml, */*\r\n" + \
              "\r\n"
    return content


def tcp(host, port, content, duration=1):
    """
    Tcp 連線壓力測試。

    :param host: 目標主機。
    :param port: 目標端口。
    :param content: 應用層封包內容。
    :param duration: 持續時間，秒，默認為 1。
    :return: None.
    """
    endtime = time.time() + duration
    print(" Hajimaruyo!~ ".center(60, '='))
    print("target: {0}:{1} ... ".format(host, port))

    # Flood loop
    while endtime >= time.time():
        # IPv4 + TCP
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, port))
            s.sendall(content.encode("UTF-8"))
        except Exception as exc:
            return exc

        # 測試伺服器是否會回應該請求。
        # rdata = s.recv(RECEIVE_BUFFER_SIZE)
        # print(" MSG ".center(60, "="))
        # print(rdata.decode("UTF-8"))
        # print(" REPR ".center(60, "="))
        # print(repr(rdata.decode("UTF-8")))
        # print(" MSG END ".center(60, "="))

        # 關閉這個連線。
        # s.close()

    print(" Done!~ ".center(60, '='))
    return None


def main(argv):
    import argparse

    parser = argparse.ArgumentParser(description='Server stress testing.')
    parser.add_argument('host', type=str, help='Target ip.')
    parser.add_argument('port', type=int, help='Target port.')
    parser.add_argument('time', type=int, nargs='?', default=10, help='Duration time, sec, default: 10s.')

    args = parser.parse_args(argv)
    print(args.host, args.port, args.time)

    tcp(args.host, args.port, http_get(args.host), args.time)


if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
