def tcp_up(host, port, delay=1.0):
    """
    :return: True/False if tcp port is up.
    """
    import socket

    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # IPv4, TCP
        sock.settimeout(delay)
        result = sock.connect_ex((host, port))
        sock.close()
        if result == 0:
            return True
        else:
            return False
    except socket.error as e:
        print(str(e))
        return False


def scan_tcp_ports(host: list, port: list, delay=1.0):
    """
    :return: A list of host and opened port: ["host", [port, ...]]
    """
    import ipaddress

    opened = []
    for h in host:
        if isinstance(h, ipaddress.IPv4Address):
            h = str(h)

        opened_port = []
        for p in port:
            if tcp_up(h, p, delay):
                opened_port.append(p)
        if len(opened_port) > 0:
            opened.append([h, opened_port])

    return opened


def get_friends_list():
    """
    Get someone should be ignored.
    :return: A list of IPv4Address(ip)
    """
    return []


def scan_subnet_ssh(friends_list: list):
    """
    Scan all host in the subnet who SSH server opened,
        beside someone in the friends_list.
    :param friends_list: A list of IPv4Address(ip), normally return by get_friends_list().
    :return: A list of host who SSH server opened: ["host", [22]]
    """
    # import ipaddress
    import get_ifs_ipv4

    _ifs = get_ifs_ipv4.get_ifs_ipv4()
    # _ifs: a list of each interface info: ["if_name", IPv4Address(self_ip), IPv4Network(self_subnet)]
    opened = []
    for _if in _ifs:
        if _if[1] not in friends_list:
            friends_list.append(_if[1])
        hosts = list(_if[2].hosts())
        hosts = filter(lambda h: h not in friends_list, hosts)
        opened.extend(scan_tcp_ports(list(hosts), [22], 0.05))

    return opened


if __name__ == '__main__':
    import os
    import json

    file_path = os.path.abspath(os.path.dirname(__file__))

    open_port = scan_subnet_ssh(get_friends_list())
    print(open_port)

    with open(os.path.join(file_path, "opened_ssh.json"), 'w', encoding="UTF-8") as f:
        json.dump(open_port, f)
