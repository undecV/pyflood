#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
A Simple TCP Server

Receive message and print it.
Nothing else :)
"""

import sys
import time
import socket


__author__ = "undecV"


SERVER_ADDR = "0.0.0.0"
SERVER_PORT = 9488
RECEIVE_BUFFER_SIZE = 2048


def main(_):
    # IPv4 + TCP
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((SERVER_ADDR, SERVER_PORT))
    s.listen()

    # Server loop
    while True:
        cs, caddr = s.accept()
        print("{0} | Client: {1}".format(time.asctime(time.localtime(time.time())), caddr))
        data = cs.recv(RECEIVE_BUFFER_SIZE)
        print(" UTF-8 ".center(60, "="))
        print(data.decode("UTF-8"))
        print(" REPR() ".center(60, "="))
        print(repr(data.decode("UTF-8")))
        print(" END ".center(60, "="))
        # break

    # cs.close()
    # s.close()


if __name__ == '__main__':
    main(sys.argv)
