def get_yaml(file):
    import yaml  # pip install pyyaml

    with open(file, "r", encoding="UTF-8") as f:
        data = yaml.load(f.read())
        # print("Load \"{}\" succeed.".format(file))
        return data
