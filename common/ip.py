#!/usr/bin/python
# -*- coding: UTF-8 -*-


def validate_ipv4(ip):
    _bytes = ip.split('.')
    if len(_bytes) != 4:
        return False
    for _byte in _bytes:
        if not _byte.isdigit():
            return False
        i = int(_byte)
        if i < 0 or i > 255:
            return False
    return True


def validate_port(port):
    if isinstance(port, str):
        if port.isdecimal() is not True:
            return False
        else:
            port = int(port)

    if ((port >= 0) and (port <= 65535)) is not True:
        return False

    return True


def local_ip():
    """
    Find local ip address.
    Ref: https://stackoverflow.com/questions/11735821/python-get-localhost-ip

    :return: Local ip address.
    """
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)   # UDP
    s.connect(("10.0.0.0", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip


if __name__ == '__main__':
    pass
