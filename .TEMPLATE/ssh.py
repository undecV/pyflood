# !/usr/bin/env python
# -*- coding: utf-8 -*-

import paramiko


def ssh_cmd(host, port, username, password, cmd):
    """A ssh client, login and execute commands."""
    ssh = paramiko.SSHClient()
    # ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    cmd_str = ";".join(cmd)
    ssh.connect(host, port, username, password)
    stdin, stdout, stderr = ssh.exec_command(cmd_str)
    print(stdout.read().decode("UTF-8"))
    ssh.close()
    return


def main():
    host = "192.168.0.205"
    port = 22
    username = "pi"
    password = "raspberry"
    cmd = ["uptime", "ls -al \Downloads"]
    try:
        ssh_cmd(host, port, username, password, cmd)
    except:
        print("SSH Failed.")


if __name__ == "__main__":
    main()
