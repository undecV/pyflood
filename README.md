# README

## 環境需求 - Environment

- 控制端（Master）：Windows or Linux, python 3.x.
- 受控端（Maids）: Linux, python 3.x.
- Modules:

```python
import paramiko  # pip install paramiko
import colorama  # pip install colorama
```

## Files

- 滲透程式：控制殭屍電腦，執行指令
    - `re_server.py`：反向 Shell 伺服器
    - `re_client.py`：反向 Shell 客戶端（後門程式）
- 擴散程式：掃描子網絡下的電腦，並加入到殭屍網絡中
    - `get_ifs_ipv4.py`: 取得電腦所有網路卡的子網路的訊息
    - `pass_dict.yaml`：弱口令字典
    - `scan_port.py`：端口掃描工具
    - `scan_ssh.py`：掃描 SSH 的默認帳密
- 攻擊程式：執行 Flooding 攻擊
    - `flood.py`：~~伺服器壓力測試工具~~，Flooding 攻擊，可獨立執行
- 其他
    - `common` 實用函數大雜燴
        - `common.files_io`：檔案存取
        - `common.ip`: 檢查字串是否是 IP 的格式、找到自己的 IP 地址
